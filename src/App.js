import './App.css';
import {useState} from 'react'
import {UserProvider} from './UserContext'
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home'
import Products from './pages/Products'
import ProductView from './components/ProductView'
import Leathers from './pages/Leathers'
import Rubbers from './pages/Rubbers'
import Cart from './pages/Cart'
import Bags from './pages/Bags'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Admin from './pages/Admin'
import AddProduct from './pages/AddProduct'
import AdminProducts from './pages/AdminProducts'
import AdminProdView from './components/AdminProdView'
import ErrorPage from './pages/ErrorPage'
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
    <>
      {/*Provides the user context throughout any component inside of it*/}
      <UserProvider value={{user, setUser, unsetUser}}>
        {/*Initializes that dynamic routing will be involved*/}
        <Router>
            <AppNavbar/>
            <Container>
              <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/products" element={<Products/>}/>
                <Route path="/products/:productId" element={<ProductView/>}/>
                <Route path="/leathers" element={<Leathers/>}/>
                <Route path="/rubbers" element={<Rubbers/>}/>
                <Route path="/bags" element={<Bags/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="/register" element={<Register/>}/>
                <Route path="/logout" element={<Logout/>}/>
                <Route path="/admin" element={<Admin/>}/>
                <Route path="/addproduct" element={<AddProduct/>}/>
                <Route path="/adminproducts" element={<AdminProducts/>}/>
                <Route path="/adminproducts/:productId" element={<AdminProdView/>}/>
                <Route path="*" element={<ErrorPage/>}/>
                <Route path="/cart" element={<Cart/>}/>
              </Routes>
            </Container>
        </Router>

      </UserProvider>
    </> 
  )
}



export default App;
