import {useEffect, useState} from 'react'
import { Row, Col, Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'
import sampleimage from './himages/Sampleimage.png'

export default function ProductCard({product}) {

  // Destructuring the props
    const {productName, brand, description, price, _id} = product

  // Initialize a 'count' state with a value of (0)
    const [count, setCount] = useState(0)
    const [slots, setSlot] = useState(15)
    const [isOpen, setIsOpen] = useState(true)

    return (
      
      <div className="prodrow ">
      <Card className="cardProducts" >
        <Card.Body>
          <img className="prodimages" src={sampleimage} alt="Product Image"/>
          <h2>{productName}</h2>
          <h2>Brand: {brand}</h2>  
          <h4>Price: Php {price}</h4>
          <Card.Title>Description:</Card.Title>
          <Card.Text>
            {description}
          </Card.Text>
          <Link id="detailsbtn"className=" btn btn-primary" to={`/products/${_id}`}>Details</Link>
        </Card.Body>
      </Card>
      </div>
      
    );
}

// Prop Types can be used to validate the data coming from the props. You can define each property of the props and assign specific validation for each of them
ProductCard.propTypes = {
  product: PropTypes.shape({
    productName: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  })
}