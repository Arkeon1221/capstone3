import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink } from 'react-router-dom'
import {useContext} from 'react'
import UserContext from '../UserContext'
import Container from 'react-bootstrap/Container';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Register from '../pages/Register'
import merqatologo from './himages/merqatologo.png'
import shopbag from './himages/shopbag.png'

function AppNavBar() {

  const {user} = useContext(UserContext)

  return (
    <Navbar className="sticky-top" collapseOnSelect expand="lg" bg="dark" variant="dark">
      <Container>

        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>

            {(user.isAdmin) ?

             <NavDropdown className="navdrop" title="Admin Tools" id="collasible-nav-dropdown">
              <NavDropdown.Item as={NavLink} to="/addproduct">Add New Product</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={NavLink} to="/adminproducts">View All Products</NavDropdown.Item>
              {/*<NavDropdown.Item as={NavLink} to="/addprodcard">Update Product</NavDropdown.Item>*/}
              <NavDropdown.Item href="#action/3.4">View All Customers</NavDropdown.Item>
            </NavDropdown>

            :


            <NavDropdown className="navdrop" title="Products" id="collasible-nav-dropdown">
              <NavDropdown.Item as={NavLink} to="/leathers">Leather Shoes</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/rubbers">Rubber Shoes</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/bags">Bags and Accessories</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item as={NavLink} to="/products">All Products</NavDropdown.Item>
            </NavDropdown>
          }
          </Nav>



          <Nav as={Link} to="/" alt="Project Image">
            <img className="logo" src={merqatologo}  />
          </Nav>




          <Nav>

            { (user.id !== null) ?

            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            :
            <>
              <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
              <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            </>
            }

            

          </Nav>

         

        </Navbar.Collapse>

        {/*<Nav as={Link} to="/" alt="Project Image">*/}
          {/*<img className="logo2" src={merqatologo}  />*/}
        {/*</Nav>*/}

        {(user.isAdmin) ?
          <>
          </>
          :


        <Navbar.Brand as={Link} to="/cart" alt="Project Image">
         <img className="shopbag" src={shopbag}  />
        </Navbar.Brand>
        }

         


      </Container>

     

    </Navbar>

  );
}

export default AppNavBar;































/*export default function AppNavBar(){

	const {user} = useContext(UserContext)

	return(
		<Navbar bg="light" expand="lg">
			<Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
					{	(user.id) ?
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
						:
						<>
							<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
							<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						</>
					}
					
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}*/