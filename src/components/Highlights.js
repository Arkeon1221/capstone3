import { Row, Col, Card } from 'react-bootstrap';
import leather from './himages/Leather.gif'
import rubber from './himages/Nike.gif'
import bag from './himages/bag.gif'

export default function Highlights(){
	return(
		<Row className="mt-0 mb-3">
			<Col xs={12} md={4}>
					<Card className="cardHighlight p-3 border-0">
					                    <Card.Body>
					                        <Card.Title>
					                            <h2>Elegance</h2>
					                            <img className="himages" src={leather} alt="Project Image"/>
					                        </Card.Title>
					                        		<Card className="cardHighlight p-2 border-0 description">
					                        		    <Card.Body>
					                        		        <Card.Title>
					                        		                <h3>Leather Shoes</h3>  
					                        		        </Card.Title>
					                        		        <Card.Text>
					                        		            Elegance can be a really positive thing.

Whilst some people don't like the idea of elegance as they think it represents outdated thinking and doesn't allow girls and women to be strong, others love acting elegantly in a modern way. Clothes, footwear, and accessories, although often associated with it, are not the important factors that affect elegance, as elegance comes from the inner self.

In order to be classy, you need to be elegant and graceful. Elegance can be achieved by smiling more, speaking clearly, and by dressing in a way that you feel comfortable. Kindness is also another important value of any elegant woman or man.
					                        		        </Card.Text>
					                        		    </Card.Body>
					                        		</Card>
					                        <Card.Text>
					                            
					                        </Card.Text>
					                    </Card.Body>
					</Card>
			 </Col>
			 <Col xs={12} md={4}>
					                <Card className="cardHighlight p-3 border-0 ">
					                    <Card.Body>
					                        <Card.Title>
					                            <h2>Energy</h2>
					                            <img className="himages" src={rubber} alt="Project Image"/>
					                        </Card.Title>
					                        <Card className="cardHighlight p-2 border-0 description">
					                        	<Card.Body>
					                        		<Card.Title>
					                        				<h3>Rubber Shoes</h3>  
					                        		</Card.Title>
					                        		<Card.Text>
					                        			Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
					                        		</Card.Text>
					                        	</Card.Body>
					                        </Card>
					                    </Card.Body>
					                </Card>
			 </Col>
			 <Col xs={12} md={4}>
					                <Card className="cardHighlight p-3 border-0">
					                    <Card.Body>
					                        <Card.Title>
					                            <h2>Style</h2>
					                            <img className="himages" src={bag} alt="Project Image"/>
					                        </Card.Title>
					                        <Card className="cardHighlight p-2 border-0 description">
					                        	<Card.Body >
					                        		<Card.Title>
					                        				<h3>Bags and Accessories</h3>  
					                        		</Card.Title>
					                        		<Card.Text>
					                        			Pariatur adipisicing aute do amet dolore cupidatat. Eu labore aliqua eiusmod commodo occaecat mollit ullamco labore minim. Minim irure fugiat anim ea sint consequat fugiat laboris id. Lorem elit irure mollit officia incididunt ea ullamco laboris excepteur amet. Cillum pariatur consequat adipisicing aute ex.
					                        		</Card.Text>
					                        	</Card.Body>
					                        </Card>
					                    </Card.Body>
					                </Card>
			 </Col>
		</Row>
	)
}