import {Button, Row, Col} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import React from "react";
import merqatobanner from './himages/merqatobanner.png'

export default function AdminBanner(){
	return (

		<row className="banner d-lg-flex p-md-1">
		<img className="merqatobanner" src={merqatobanner}  />
		
		<h5 className="tagline">Merqato! ADMIN page</h5>
		
  </row>

  
);
}