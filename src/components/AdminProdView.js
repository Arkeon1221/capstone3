
import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import {useParams, Navigate, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2'

export default function AdminProdView(){

	const {productId} = useParams()	
 const {user} = useContext(UserContext)

  const navigate = useNavigate()

  const [productName, setProductName] = useState('')
  const [brand, setBrand] = useState('')
  const [category, setCategory] = useState('')
  const [price, setPrice] = useState(null)
  const [stocks, setStocks] = useState('')
  const [description, setDescription] = useState('')
 // For determining if the button is disabled or not
  const [isActive, setIsActive] = useState(null)
  const [active, setActive] = useState(null)

  function updateProduct(event){
    event.preventDefault()

     fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        // Convert a JavaScript object into a string 
        body: JSON.stringify({
          productName: productName,
          brand: brand,
          category: category,
          price: price,
          stocks: stocks,
          description: description
        })
      })


     	 .then(response => response.json())
     	 .then(result => {
     	   if(result !== false){


     	     Swal.fire({
     	       title: `Product has been updated!`,
     	       icon: "succes",
     	       text: "Success! 😊"
     	     })
     	     // redirects to adminproducts page
     	     // (isActive === null)
     	     navigate('/adminproducts')

     	   } else {

     	       Swal.fire({
     	         title: "Something went wrong!",
     	         icon: "error",
     	         text: "Please try again! :("
     	       })

     	     }

     	 })

  }

     function deactivate() {

     	 fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        // Convert a JavaScript object into a string 
        body: JSON.stringify({
       
          isActive: isActive
         
        })
      })

     	 .then(response => response.json())
     	 .then(result => {
     	   if(result !== false){


     	     Swal.fire({
     	       title: `Product has been Deactivated!`,
     	       icon: "succes",
     	       text: "Success! 😊"
     	     })
     	     // redirects to adminproducts page
     	     // (isActive === null)
     	     navigate('/adminproducts')

     	   } else {

     	       Swal.fire({
     	         title: "Something went wrong!",
     	         icon: "error",
     	         text: "Please try again! :("
     	       })

     	     }

     	 })

     } 

     		function activate() {


     	 fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          "Authorization": `Bearer ${localStorage.getItem('token')}`
        },
        // Convert a JavaScript object into a string 
        body: JSON.stringify({
       
          isActive: isActive
         
        })
      })

     	 .then(response => response.json())
     	 .then(result => {
     	   if(result !== false){


     	     Swal.fire({
     	       title: `Product has been Activated!`,
     	       icon: "succes",
     	       text: "Success! 😊"
     	     })
     	     // redirects to adminproducts page
     	     navigate('/adminproducts')

     	   } else {

     	       Swal.fire({
     	         title: "Something went wrong!",
     	         icon: "error",
     	         text: "Please try again! :("
     	       })

     	     }

     	 })

     }
     
  
useEffect(() => {

				fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/retrieve`)
				.then(response => response.json())
				.then(result => {
					setProductName(result.productName)
					setBrand(result.brand)
					setCategory(result.category)
					setIsActive(result.isActive)
					setDescription(result.description)
					setPrice(result.price)
					setStocks(result.stocks)
				})
			}, [productId])


			return(


				<Form id="regform" className=" card col-lg-4 col-med-3 col-sm-7 col-12 " onSubmit={event => updateProduct(event)}>
				<Form.Label  className="text-center">Merqato! Product Editor</Form.Label>
				  <Form.Group controlId="Product Entry" className="productEntry" >
				  <Form.Label>Edit Product Name:</Form.Label>
				      
				      <Form.Control 
				      
				          type="text" 
				          placeholder="Enter Product Name" 
				          value={productName}
				          onChange={event => setProductName(event.target.value)}
				          required
				      />
				  </Form.Group>

				  <Form.Group controlId="brand">
				      <Form.Label>Edit Brand:</Form.Label>
				      <Form.Control 
				          type="text" 
				          placeholder="Enter Brand" 
				          value={brand}
				          onChange={event => setBrand(event.target.value)}
				          required
				      />
				  </Form.Group>

				  <Form.Group controlId="category">
				      <Form.Label>Edit Category:</Form.Label>
				      <Form.Select value={category} 
				                  onChange={event => setCategory(event.target.value)}>
				        <option>Choose...</option>
				        <option value="Leather Shoes">Leather Shoes</option>
				        <option value="Rubber Shoes">Rubber Shoes</option>
				        <option value="Bags and Accessories">Bags and accessories</option>
				      </Form.Select>
				  </Form.Group>

  
				  	<Form.Group controlId="See Status" className="seestatus" >
			 			<Form.Label>Current status:</Form.Label>

						  		{(stocks <= 0) ?

 				  			<Form.Control 
				      		id="noStock"
				          type="text" 
				          disabled
				          value="This product has no more stocks. Please add more stocks!"
				          
				      	/>

				      	:
				      	<>
				      				{(isActive) ?

				      		    <Form.Control 
				      		    		id="activecolor"
				      		        type="text" 
				      		        disabled
				      		        value="ACTIVE"
				      		        
				      		    />
				      		    :

				      		    <Form.Control 
				      		        id="inactivecolor"
				      		        type="text" 
				      		        disabled
				      		        value="INACTIVE"
				      		        color="red"
				      		    />
				      		  }



				      	</>
 				  		}

				  		
			  </Form.Group>

			  				    <>
			  				  	     <div>

			  				  	      {(stocks <= 0) ?


			  				  	        <Button  disabled className="statusbutton" variant="success" size="sm">
			  				  	          Reactivate
			  				  	        </Button>

			  				  	        :

			  				  	        <>
			  				  	        			{(isActive) ?

			  				  	        				<div className="buttons-container">
			  				  	        				       
			  				  	        				       <a onClick={status1 => deactivate()} className="btn test-completed"></a>
			  				  	        				   </div>

			   				  	        			  //<Button onClick={status1 => deactivate()} className="btn1 statusbutton1"variant="danger" size="sm" >
			   				  	        			    
			   				  	        			  //</Button>

			   				  	        			  :

			   				  	        			  <Button onClick={status2 => activate()} className="statusbutton" variant="success" size="sm">
			  				  	        			    Reactivate
			   				  	        			  </Button>

			   				  	        			}

			   				  	        </>

			   				  	     }
			   				  	    </div>				   
			   				 </>


				  <Form.Group controlId="price">
				      <Form.Label>Edit Price:</Form.Label>
				      <Form.Control 
				          type="number" 
				          placeholder="Set Price ₱" 
				          value={price}
				          onChange={event => setPrice(event.target.value)}
				          required
				      />
				      <Form.Text className="text-muted">
				      </Form.Text>
				  </Form.Group>

				  <Form.Group controlId="stocks">
				      <Form.Label>Edit Number of Stocks:</Form.Label>
				      <Form.Control 
				          type="number" 
				          placeholder="Enter number of stocks" 
				          value={stocks}
				          onChange={event => setStocks(event.target.value)}
				          required
				      />
				  </Form.Group>

				  <Form.Group controlId="description">
				      <Form.Label>Edit Product Description:</Form.Label>
				      <Form.Control 
				          as="textarea" 
				          placeholder="Type description here..." 
				          value={description}
				          onChange={event => setDescription(event.target.value)}
				          required
				      />
				  </Form.Group>

				  <>
				  
				  	      <div id="confirmbuttons">
				  	        <Button className="mx-2"variant="primary" size="sm" type="submit">
				  	          Update
				  	        </Button>
				  	        <Button as={Link} to={`/adminproducts`} className="mx-2" variant="secondary" size="sm">
				  	          Cancel
				  	        </Button>
				  	      </div>
		   
				 </>
				   
				  </Form>

			)
			
		}




