import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import {useParams, useNavigate, Navigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import sampleimage from './himages/Sampleimage.png'

		export default function ProductView() {

			
			// Gets the productId from the URL of the route that this component is connected to. '/products/:productId'
			const {productId} = useParams()

			const {user} = useContext(UserContext)
			const navigate = useNavigate()

			const [productName, setProductName] = useState("");
			const [description, setDescription] = useState("");
			const [brand, setBrand] = useState("");
			const [price, setPrice] = useState(0);


			const checkout = (productId) => {
				fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
					method: 'POST',
					headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						productId: productId,
						userId: user.id
					})
				})

				.then(response => response.json())
				.then(result => {
					if(result){
						Swal.fire({
							title: "Success!",
							icon: "succes",
							text: "Purchase successful!"
						})

						navigate('/products')

					} else {
						Swal.fire({
							title: "Something went wrong!",
							icon: "error",
							text: "Please try again! :("
						})
					}
				})
			}

			useEffect(() => {

				fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/retrieve`)
				.then(response => response.json())
				.then(result => {
					setProductName(result.productName)
					setBrand(result.brand)
					setDescription(result.description)
					setPrice(result.price)
				})
			}, [productId])

			return(

				<Container className=" prodcontainer ">
					<Row>
						<Col lg={{ span: 5, offset: 4 }}>

							<Card>
								<Card.Body className="text-center checkoutcard">
									<img className="prodimages" src={sampleimage} alt="Product Image"/>
									<h2>{productName}</h2>
									<h2>Brand: {brand}</h2>
									<h4>Price: Php {price}</h4>
									<h4 className="prodesc">Description:</h4>

									<Card.Text className="prodesbody">{description}</Card.Text>

									<Card.Subtitle></Card.Subtitle>
									<Card.Text></Card.Text>
									{	user.id !== null ?
											<Button variant="primary" onClick={() => checkout(productId)} exact="true">Buy</Button>
										:
											<Link className="btn btn-danger btn-block" to='/login'>Login to buy!</Link>
									}
									<Card.Text></Card.Text>
									<Card.Text></Card.Text>
								</Card.Body>		
							</Card>
						</Col>
					</Row>
				</Container>
			)
		}