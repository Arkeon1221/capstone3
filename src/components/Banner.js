import {Button, Row, Col} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import React from "react";
import merqatobanner from './himages/merqatobanner.png'

export default function Banner(){
	return (

		<row className="banner d-lg-flex p-md-1">
		<img className="merqatobanner" src={merqatobanner}  />
		
		<h5 className="tagline">Your best online shopping experience!</h5>
		<Button  id="bannerbtn" variant="primary" as={NavLink} to="/products">Shop now!</Button>
  </row>

  
);
}
		