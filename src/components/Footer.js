import React from "react";

const Footer = () => (
  <div className="footer">
    <p>©2022 | Ronald Matanguihan | All Rights Reserved</p>
  </div>
);

export default Footer;