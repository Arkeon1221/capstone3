import {Button, Row, Col} from 'react-bootstrap'
import AdminBanner from '../components/AdminBanner';
import Highlights from '../components/Highlights';
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import {useContext} from 'react'
import Swal from 'sweetalert2'


export default function Admin(){

	const {user, setUser} = useContext(UserContext)

	return(

		(user.isAdmin) ?

		<>
			
			{/*<Navigate to="/admin"/>*/}
			<AdminBanner/>
			<Highlights/>
			
		</>

		:

		<>
			{

     	       Swal.fire({
     	         title: "Please Login as ADMIN!",
     	         icon: "error",
     	         text: "This is for ADMIN only!"
     	       })

     	     }
			
			<Navigate to="/login"/>
			
		</>

	)
}