import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Login(){
	// Initializes the use of the properties from the UserProvider in App.js file
	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [firstName, setFirstName] = useState('')

	// Initialize useNavigate
	const navigate = useNavigate()
	
	const [isActive, setIsActive] = useState(false)

	const retrieveUser = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`,
				'Content-Type': 'application/json'
			}
		})
		.then(response => response.json())
		.then(result => {

			// Store the user details retrieved from the token into the global state
			setUser({
				id: result._id,
				isAdmin: result.isAdmin,
				firstName: result.firstName
			})
		})
	}

	function authenticate(event){
		event.preventDefault()
		


		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password,
				firstName: firstName
			})
		})
		.then(response => response.json())
		.then(result => {

			if(typeof result.accessToken !== "undefined"){
				localStorage.setItem('token', result.accessToken)

				retrieveUser(result.accessToken)
				// const firstName = event.firstName

				Swal.fire({
					title: 'Login Successful!',
					icon: 'success',
					text: `Welcome to our store ${firstName}!`

				})

			} else {

					Swal.fire({
					title: 'Login failed!',
					icon: 'error',
					text: 'Please create an account...'

				})
			}
		})

	}

	

	useEffect(() => {
		if(email !== '' && password !== '' ){
			
			setIsActive(true)
		} else {
				setIsActive(false)
		}
	}, [email, password])
	
	return (

			(user.isAdmin) ?

				<Navigate to="/admin"/>
			:
	
			(user.id !== null) && (!user.isAdmin) ?
				<Navigate to="/products"/>
			:

				<Form id="regform" className=" card col-lg-4 col-med-3 col-sm-7 col-12 mt-5 pt-4" onSubmit={event => authenticate(event)}>

					<Form.Group controlId="userEmail">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control 
					        type="email" 
					        placeholder="Enter email" 
					        value={email}
					        onChange={event => setEmail(event.target.value)}
					        required
					    />
					    <Form.Text className="text-muted">
					        Please enter your email to Login!
					    </Form.Text>
					</Form.Group>

					<Form.Group controlId="password1">
					    <Form.Label>Password</Form.Label>
					    <Form.Control 
					        type="password" 
					        placeholder="Password" 
					        value={password}
					        onChange={event => setPassword(event.target.value)}
					        required
					    />
					</Form.Group>

					{	isActive ? 
						<Button variant="primary" type="submit" id="submitBtn">
						Submit
						</Button>
						:
						<Button variant="primary" type="submit" id="submitBtn" disabled>
						Submit
						</Button>
					}
					

					</Form>
		
		
	)

}