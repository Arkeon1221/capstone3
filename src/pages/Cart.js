import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'
import {useEffect, useState} from 'react'
import { Row, Col, Card, Button } from 'react-bootstrap';
import Table from 'react-bootstrap/Table';
import CartCard from '../components/CartCard'


export default function Cart() {


    <row>
      <Table striped bordered hover className="admintable" responsive="sm" responsive="md">
          <thead>
            <tr>
              <th>Product ID:</th>
              <th>Product Name:</th>
              <th>Category:</th>
              <th>Brand:</th>
              <th>Price:</th>
              <th>Stocks left:</th>
              <th className="actions">Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              {/*<td>#{_id}</td>
              <td>{productName}</td>
              <td>{category}</td>
              <td>{brand}</td>
              <td>Php {price}</td>
              <td>{stocks}</td>
              <td >
              <Button size='sm' as={Link} to={`/adminproducts/${_id}`}>Update</Button>

              
              </td>*/}
            </tr>
            
            
          </tbody>
        </Table>


    </row>
      

        
      

}
  







//Prop Types can be used to validate the data coming from the props. You can define each property of the props and assign specific validation for each of them
CartCard.propTypes = {
  product: PropTypes.shape({
    productName: PropTypes.string.isRequired,
    brand: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    stocks: PropTypes.number.isRequired
  })
}