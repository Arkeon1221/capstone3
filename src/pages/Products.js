import ProductCard from '../components/ProductCard'
import {useEffect, useState} from 'react'
import Loading from '../components/Loading'


export default function Products(){

	const [products, setProducts] = useState([])
	const [category, setCategory] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {

		// Sets the loading state to true
		setIsLoading(true)

		fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
		.then(response => response.json())
		.then(result => {
			setProducts(
					result.map(product => {

							return (
								<ProductCard  key={product._id} product={product} />


							)
						})
				)
			// Sets the loading state to false
			setIsLoading(false)
		})
	}, [])

	return(

		(isLoading) ?
			<Loading className="loading"/>
		:
			<>	
				<div  className="productlayout">
					{products}

				</div>
					

			</>
	)
}