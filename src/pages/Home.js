import Banner from '../components/Banner';
import AdminBanner from '../components/AdminBanner';
import Highlights from '../components/Highlights';
import {useEffect, useState} from 'react'
import UserContext from '../UserContext'
import {useContext} from 'react'
import {Navigate, useNavigate} from 'react-router-dom'




export default function Home(){

	const {user, setUser} = useContext(UserContext)

	
	return(

		(user.isAdmin) ?

		<>
			
			<Navigate to="/admin"/>
			
		</>

		:

		<>
			
			<Banner/>

			<Highlights/>
			
		</>
	)
}