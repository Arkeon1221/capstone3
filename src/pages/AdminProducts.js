import AdminProdCard from '../components/AdminProdCard'
import AdminDashboard from '../components/AdminDashboard'
import {useEffect, useState} from 'react'
import Loading from '../components/Loading'


export default function AdminProducts(){



	const [adminproducts, setAdminProducts] = useState([])
	const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {

		// Sets the loading state to true
		setIsLoading(true)



		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(response => response.json())
		.then(result => {
			setAdminProducts(
					result.map(adminproduct => {

							return (

								<AdminProdCard key={adminproduct._id} adminproduct={adminproduct}/>

							)
						})
				)
			// Sets the loading state to false
			setIsLoading(false)
		})
	}, [])

	return(

		(isLoading) ?
			<Loading />
		:
			<>	
				<AdminDashboard/>
				{adminproducts}	
			</>
	)
}