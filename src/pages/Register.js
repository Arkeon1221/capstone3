
import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register(){

  const {user} = useContext(UserContext)

  const navigate = useNavigate()

  const [email, setEmail] = useState('')
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [mobileNumber, setMobileNumber] = useState('')

 

  // For determining if the button is disabled or not
  const [isActive, setIsActive] = useState(false)

  function registerUser(event){
    event.preventDefault()

    // Backend connection
    fetch(`${process.env.REACT_APP_API_URL}/users/check-email`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      // Convert a JavaScript object into a string 
      body: JSON.stringify({
        email: email
      })
    })
    .then(response => response.json())
    .then(result => {
      if(result === true){
        Swal.fire({
          title: 'Ooops!',
          icon: 'error',
          text: "There's an account with that email already!"
        })
      } else{
        // Register request

      fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        // Convert a JavaScript object into a string 
        body: JSON.stringify({
          firstName: firstName,
          lastName: lastName,
          mobileNumber: mobileNumber,
          email: email,
          password: password1
        })
      })
      .then(response => response.json())
      .then(result => {
        if(result !== false){

          // Clears out the input fields after the form submission
          setFirstName('')
          setLastName('')
          setMobileNumber('')
          setEmail('')
          setPassword1('')
          setPassword2('')

          Swal.fire({
            title: `Welcome ${firstName}!`,
            icon: "succes",
            text: "Your account has been created! 😊"
          })
          // redirects to Login page
          navigate('/login')

        } else {

            Swal.fire({
              title: "Something went wrong!",
              icon: "error",
              text: "Please try again! :("
            })

          }

      })

      }
    })    
  }

  useEffect(() => {
    if((firstName !== '' && lastName !== '' && mobileNumber.length !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
      // Enables the submit button if the form data has been verified
      setIsActive(true)
    } else {
        setIsActive(false)
    }
  }, [firstName, lastName, mobileNumber, email, password1, password2])
  
  return (


      // (user._id !== null) ?

      //   <Navigate to="/products"/>
      // :

    <Form id="regform" className=" card col-lg-4 col-med-3 col-sm-7 col-12 " onSubmit={event => registerUser(event)}>
    <Form.Label className="text-center">CREATE ACCOUNT</Form.Label>
      <Form.Group controlId="userEmail" className="firstname">
          <Form.Label>First Name</Form.Label>
          <Form.Control 
              type="text" 
              placeholder="Enter First Name" 
              value={firstName}
              onChange={event => setFirstName(event.target.value)}
              required
          />
      </Form.Group>

      <Form.Group controlId="lastName">
          <Form.Label>Last Name</Form.Label>
          <Form.Control 
              type="text" 
              placeholder="Enter Last Name" 
              value={lastName}
              onChange={event => setLastName(event.target.value)}
              required
          />
      </Form.Group>

      <Form.Group controlId="mobileNumber">
          <Form.Label>Mobile Number</Form.Label>
          <Form.Control 
            id = "phone"
              type="text" 
              placeholder="Enter Mobile Number" 
              value={mobileNumber}
              onChange={event => setMobileNumber(event.target.value)}
              required
          />
      </Form.Group>

      <Form.Group controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control 
              type="email" 
              placeholder="Enter email" 
              value={email}
              onChange={event => setEmail(event.target.value)}
              required
          />
          <Form.Text className="text-muted">
              We'll never share your email with anyone else.
          </Form.Text>
      </Form.Group>

      <Form.Group controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control 
              type="password" 
              placeholder="Password" 
              value={password1}
              onChange={event => setPassword1(event.target.value)}
              required
          />
      </Form.Group>

      <Form.Group controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control 
              type="password" 
              placeholder="Verify Password" 
              value={password2}
              onChange={event => setPassword2(event.target.value)}
              required
          />
      </Form.Group>

      { isActive ? 
        <Button variant="primary" type="submit" id="submitBtn">
        Submit
        </Button>
        :
        <Button variant="primary" type="submit" id="submitBtn" disabled>
        Submit
        </Button>
      }
       
      </Form>

  )

}



