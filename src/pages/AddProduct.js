import {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Modal from 'react-bootstrap/Modal';
import Swal from 'sweetalert2'

export default function AddProduct(){

 const {user} = useContext(UserContext)

  const navigate = useNavigate()

  const [productName, setProductName] = useState('')
  const [brand, setBrand] = useState('')
  const [category, setCategory] = useState('')
  const [price, setPrice] = useState('')
  const [stocks, setStocks] = useState('')
  const [description, setDescription] = useState('')
 // For determining if the button is disabled or not
  const [isActive, setIsActive] = useState(false)


  function createProduct(event){
    event.preventDefault()

     fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        // Convert a JavaScript object into a string 
        body: JSON.stringify({
          productName: productName,
          brand: brand,
          category: category,
          price: price,
          stocks: stocks,
          description: description
        })
      })

      .then(response => response.json())
      .then(result => {
        if(result !== false){

          // Clears out the input fields after the form submission
          setProductName('')
          setBrand('')
          setCategory('')
          setPrice('')
          setStocks('')
          setDescription('')

          Swal.fire({
            title: `New product added!`,
            icon: "succes",
            text: "New product has been added to your inventory! 😊"
          })
          // redirects to Login page
          navigate('/admin')

        } else {

            Swal.fire({
              title: "Something went wrong!",
              icon: "error",
              text: "Please try again! :("
            })

          }

      })
  }

      useEffect(() => {
        if(productName !== '' && brand !== '' && price !== '' && stocks !== '' && description !== '' ){
          // Enables the submit button if the form data has been verified
          setIsActive(true)
        } else {
            setIsActive(false)
        }
      }, [productName, brand, price, stocks, description])
      
      return (


          // (user._id !== null) ?
          //   <Navigate to="/logout"/>
          // :

        <Form id="regform" className=" card col-lg-4 col-med-3 col-sm-7 col-12 " onSubmit={event => createProduct(event)}>
        <Form.Label className="text-center">Merqato! Product Entry</Form.Label>
          <Form.Group controlId="Product Entry" className="productEntry">
              <Form.Label>Product Name:</Form.Label>
              <Form.Control 
                  type="text" 
                  placeholder="Enter Product Name" 
                  value={productName}
                  onChange={event => setProductName(event.target.value)}
                  required
              />
          </Form.Group>

          <Form.Group controlId="brand">
              <Form.Label>Brand:</Form.Label>
              <Form.Control 
                  type="text" 
                  placeholder="Enter Brand" 
                  value={brand}
                  onChange={event => setBrand(event.target.value)}
                  required
              />
          </Form.Group>

          <Form.Group controlId="category">
              <Form.Label>Category</Form.Label>
              <Form.Select value={category} 
                          onChange={event => setCategory(event.target.value)}>
                <option>Choose...</option>
                <option value="Leather Shoes">Leather Shoes</option>
                <option value="Rubber Shoes">Rubber Shoes</option>
                <option value="Bags and Accessories">Bags and accessories</option>
              </Form.Select>
          </Form.Group>

          <Form.Group controlId="price">
              <Form.Label>Price:</Form.Label>
              <Form.Control 
                  type="number" 
                  placeholder="Set Price ₱" 
                  value={price}
                  onChange={event => setPrice(event.target.value)}
                  required
              />
              <Form.Text className="text-muted">
              </Form.Text>
          </Form.Group>

          <Form.Group controlId="stocks">
              <Form.Label>Number of stocks:</Form.Label>
              <Form.Control 
                  type="number" 
                  placeholder="Enter number of stocks" 
                  value={stocks}
                  onChange={event => setStocks(event.target.value)}
                  required
              />
          </Form.Group>

          <Form.Group controlId="description">
              <Form.Label>Product description:</Form.Label>
              <Form.Control 
                  as="textarea" 
                  placeholder="Type description here..." 
                  value={description}
                  onChange={event => setDescription(event.target.value)}
                  required
              />
          </Form.Group>

          { isActive ? 
            <Button variant="primary" type="submit" id="submitBtn">
            Add Product
            </Button>
            :
            <Button variant="primary" type="submit" id="submitBtn" disabled>
            Add Product
            </Button>
          }
           
          </Form>

      )

}

 



